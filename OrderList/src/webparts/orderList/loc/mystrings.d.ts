declare interface IOrderListWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;

  OrderTitle1: string;
  OrderTitle2: string;
  OrderTitle3: string;

  OrderNumber1: string;
  OrderNumber2: string;
  OrderNumber3: string;

  DeleteItemConfirm: string;
}

declare module 'OrderListWebPartStrings' {
  const strings: IOrderListWebPartStrings;
  export = strings;
}

define([], function() {
  return {
    "PropertyPaneDescription": "Description",
    "BasicGroupName": "Group Name",
    "DescriptionFieldLabel": "Description Field",

    OrderTitle1: 'First Order',
    OrderTitle2: 'Second Order',
    OrderTitle3: 'Third Order',

    OrderNumber1: '123456',
    OrderNumber2: '234567',
    OrderNumber3: '345678',

    DeleteItemConfirm: 'Are you sure you want to delete this item?'
  }
});
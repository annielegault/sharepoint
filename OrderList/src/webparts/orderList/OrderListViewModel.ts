import * as ko from 'knockout';
import styles from './OrderList.module.scss';
import { IOrderListWebPartProps } from './OrderListWebPart';
require("tslib");
require("@pnp/logging");
require("@pnp/common");
require("@pnp/odata");
import { sp, List, ListEnsureResult, ItemAddResult, FieldAddResult, Item } from "@pnp/sp";

import * as strings from 'OrderListWebPartStrings';

export interface IOrderListBindingContext extends IOrderListWebPartProps {
  shouter: KnockoutSubscribable<{}>;
}

export interface OrderListItem {
  Id: number;
  Title: string;
  OrderNumber: string;
}

export default class OrderListViewModel {
  public description: KnockoutObservable<string> = ko.observable('');
  public newItemTitle: KnockoutObservable<string> = ko.observable('');
  public newItemNumber: KnockoutObservable<string> = ko.observable('');
  public items: KnockoutObservableArray<OrderListItem> = ko.observableArray([]);

  public labelClass: string = styles.label;
  public orderListClass: string = styles.orderList;
  public containerClass: string = styles.container;
  public rowClass: string = `ms-Grid-row ms-bgColor-themeDark ms-fontColor-white ${styles.row}`;
  public buttonClass: string = `ms-Button ${styles.button}`;

  constructor(bindings: IOrderListBindingContext) {
    this.description(bindings.description);

    // When web part description is updated, change this view model's description.
    bindings.shouter.subscribe((value: string) => {
      this.description(value);
    }, this, 'description');

    this.getItems().then(items => {
      this.items(items);
    });
  }

  private getItems(): Promise<OrderListItem[]> {
    return this.ensureList().then(list => {
      return list.items.select('Id', 'Title', 'OrderNumber').get<OrderListItem[]>();
    });
  }

  public addItem(): void {
    if (this.newItemTitle() !== "" && this.newItemNumber() !== "") {
      this.ensureList().then(list => {
        list.items.add({
          Title: this.newItemTitle(),
          OrderNumber: this.newItemNumber()
        }).then((iar: ItemAddResult) => {
          this.items.push({
            Id: iar.data.Id,
            OrderNumber: iar.data.OrderNumber,
            Title: iar.data.Title
          });

          this.newItemTitle("");
          this.newItemNumber("");
        });
      });
    }
  }

  public deleteItem(data): void {
    if (confirm(strings.DeleteItemConfirm)) {
      this.ensureList().then(list => {
        list.items.getById(data.Id).delete().then(_ => {
          this.items.remove(data);
        });
      }).catch((e: Error) => {
        alert(`There was an error deleting the item ${e.message}`);
      });
    }
  }

  private ensureList(): Promise<List> {
    return new Promise<List>((resolve, reject) => {
      sp.web.lists.ensure('OrderList').then((ler: ListEnsureResult) => {
        if (ler.created) {
          ler.list.fields.addText('OrderNumber').then(_ => {
            let batch = sp.web.createBatch();

            ler.list.getListItemEntityTypeFullName().then(typeName => {
              ler.list.items.inBatch(batch).add({
                Title: strings.OrderTitle1,
                OrderNumber: strings.OrderNumber1
              }, typeName);
              
              ler.list.items.inBatch(batch).add({
                Title: strings.OrderTitle2,
                OrderNumber: strings.OrderNumber2
              }, typeName);

              ler.list.items.inBatch(batch).add({
                Title: strings.OrderTitle3,
                OrderNumber: strings.OrderNumber3
              }, typeName);

              batch.execute().then(_ => {
                resolve(ler.list);
              }).catch(e => reject(e));
            }).catch(e => reject(e));
          }).catch(e => reject(e));
        }
        else 
          resolve(ler.list);
      }).catch(e => reject(e));
    });
  }
}

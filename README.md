# SharePoint OrderList

Web Part SharePoint utilisée pour le cours Séminaire au Cégep André-Laurendeau.

## Accès à SharePoint Online

Site : https://alseminaire.sharepoint.com/_layouts/15/sharepoint.aspx


Nom d'utilisateur : votreprenom@alseminaire.onmicrosoft.com

Mot de passe : voir au tableau.


## Building the code

```bash
git clone the repo
npm i
npm i -g gulp
gulp serve
```

This package produces the following:

* lib/* - intermediate-stage commonjs build artifacts
* dist/* - the bundled script, along with other resources
* deploy/* - all resources which should be uploaded to a CDN.

